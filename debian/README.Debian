webfs (1.21+ds1-4)

  At least since the release of webfs-1.21, there has been
  an error when webfsd constructs a directory listing. Files
  could have been incorrectly marked as readable, and some
  as not readable, in situations where the service is run
  with a numerical UID differing from the numerical GID.
  The reason was that in determining readability, one incorrect
  test was to compare process GID with file ownership, and in
  case the numerical values were identical, the file was marked
  as readable.
  
  Under rare circumstances, where GID was highly tailored
  to target a specific audience, it is conceivable that
  the POSIX use of ACL could have let this unwanted access
  take place. It is unlikely though.

webfs (1.21+ds1-3)

  In this release, the package Webfs has been migrated by the present
  package maintainer to use libgnutls, thereby making webfsd into an
  HTTPS-server.

  Starting Webfs as an HTTPS-server
  ---------------------------------

  Recently the configuration file '/etc/webfsd.conf' was expanded to
  include a Debconf entry "web_extra". This is suitable for activating
  the HTTPS-server. Observe that each instance of Webfsd is able to
  serve either HTTP or HTTPS on a single port, not both protocols
  in a single server instance.

  Using an entry in '/etc/webfsd.conf' like (this file is sourced by the
  init-script at startup time)

    web_extra="-S -C /etc/ssl/certs/webfsd.pem -K /etc/ssl/private/key.pem"
    web_extra="$web_extra -A /etc/ssl/certs/webfsd-ca.pem"
    web_extra="$web_extra -Q NORMAL:!AES-128-CBC"

  would enable HTTPS (using -S), with certificate (-C), key file (-K),
  CA-chain (-A), and would exclude the crypto method AES-128-CBC (-Q).

  In case the server certificate and crypto key are bundled in a single
  file, the option '-K' can be omitted.

  For particular cases the option '-V' should be useful. It makes a
  validity check of any incoming client request to be mandatory. The
  check would in particular make sure that the certificate and CA-chain
  presented by the client are in fact verifiable against the CA-chain
  active for the running Webfsd instance.

  The package includes a tool in

       /usr/share/doc/webfs/examples/create_cert.sh

  powerful enough to produce primary and secondary certificates for
  server and client, in order that experiments can be conducted to
  test the verification capabilities built into Webfs with this release.
  Since a single common key file is used, these certificated are NOT
  intended for productive use, only for testing purposes.


  Parameters for local rebuilds
  -----------------------------

  The present package has essentially been tested as an iterative server,
  but the patches were developed with threading in mind. Bug reports are
  welcome! Thus a local administrator could consider using

     # debian/Make.config

     USE_THREADS   := yes

  in a repackaging effort. There are also a few preprocessor macros that
  could be altered by the local administrator:

     #define   WEBFS_CIPHERS             "NORMAL"
     #define   DH_PARAMS_LEN             1024
     #undefine USE_TLS_NO_COMPATIBILITY

  These values are conveniently altered in CFLAGS. As they are stated
  here, they display the default settings in the source code. Observe
  that the last one is intended to switch _off_ the compatibility mode,
  as soon as the macro is defined, and which narrows co-operability with
  more obscure, or outdated client software.

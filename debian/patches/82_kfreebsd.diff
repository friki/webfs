Description: Use kernel's sendfile() with kFreeBSD.
 When compiling for GNU/kFreeBSD, check whether the
 library implements sendfile(). If so, go ahead in
 using it, otherwise fall back to the emulation.
 Only more recent glibc versions do actually provide
 a functional sendfile(), all other returning ENOSYS.
 .
 A macro DEBUG_XSENDFILE inserts debug logging
 on kFreeBSD systems.
 .
 Rename the sendfile emulation as emulsendfile()
 and keep it always available.
Author: Mats Erik Andersson <debian@gisladisker.se>
Last-Update: 2014-01-14

--- webfs-1.21+ds1.debian/response.c
+++ webfs-1.21+ds1/response.c
@@ -43,6 +43,8 @@ static inline size_t off_to_size(off_t o
     return off_bytes;
 }
 
+static ssize_t emulsendfile(int, int, off_t, off_t);
+
 #if defined(__linux__) && !defined(NO_SENDFILE)
 
 # include <sys/sendfile.h>
@@ -69,14 +71,49 @@ static ssize_t xsendfile(int out, int in
     }
     return nbytes;
 }
+
+#elif defined(__FreeBSD_kernel__) && !defined(__FreeBSD__) \
+      && !defined(NO_SENDFILE)
+
+# warning Accessing kernel sendfile() with GNU/kFreeBSD.
+# include <sys/sendfile.h>
+
+static ssize_t xsendfile(int out, int in, off_t offset, off_t off_bytes)
+{
+    size_t bytes = off_to_size(off_bytes);
+
+    off_t nbytes = sendfile(out, in, &offset, bytes);
+
+    if (nbytes < 0) {
+	/* Make sure GNU/kFreeBSD has gotten the right implementation. */
+	if (errno == ENOSYS) {
+# ifdef DEBUG_XSENDFILE
+	    /* Report only once per file transmission. */
+	    if (offset == 0)
+		syslog(LOG_NOTICE | LOG_DAEMON,
+		       "Off-loading via sendfile is not possible.");
+# endif /* DEBUG_XSENDFILE */
+	    return emulsendfile(out, in, offset, off_bytes);
+	}
+	return -1;
+    }
+    return nbytes;
+}
+
 #else
 
 # warning using slow sendfile() emulation.
 
+static ssize_t xsendfile(int out, int in, off_t offset, off_t off_bytes)
+{
+    return emulsendfile(out, in, offset, off_bytes);
+}
+#endif
+
 /* Poor man's sendfile() implementation. Performance sucks, but it works. */
 # define BUFSIZE 16384
 
-static ssize_t xsendfile(int out, int in, off_t offset, off_t off_bytes)
+static ssize_t emulsendfile(int out, int in, off_t offset, off_t off_bytes)
 {
     char buf[BUFSIZE];
     ssize_t nread;
@@ -117,8 +154,6 @@ static ssize_t xsendfile(int out, int in
     return nsent_total;
 }
 
-#endif
-
 /* ---------------------------------------------------------------------- */
 
 #if defined(USE_SSL) || defined(USE_GNUTLS)

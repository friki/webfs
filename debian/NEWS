webfs (1.21+ds1-9) unstable; urgency=low

  It has been declared a security issue CVE-2013-0347 that
  world had reading access to files in the logging directory
  "/var/log/webfs".  This has now been remedied along two paths:

  * The server has been patched to write new log files using an
    umask 0137.

  * The directory "/var/log/webfs" is now assigned a stat-override
    with value 0770.  Previously, "root $web_group 2755" was used.
    In case this override was left unchanged at the time of package
    update, the new value 0770 is being enforced unconditionally.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Fri, 25 Oct 2013 20:38:54 +0200

webfs (1.21+ds1-4) unstable; urgency=low

  An error, present at least since the release of webfs-1.21,
  has now been corrected. The issue was an incorrect comparison
  of file ownership with process GID.

  More details are given in the file README.Debian.
  
 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Thu, 22 Apr 2010 15:17:32 +0200

webfs (1.21+ds1-3) unstable; urgency=low

  Webfs is now built with support from libgnutls. Thus the server
  is able to conduct its data exchange using TLSv1 and SSLv3.
  The present implementation can be characterized as follows,
  partly deviating from the way upstream used libssl:

    * Server certificate and key file can be in separate files.
    * Presently the key cannot be protected by a pass phrase,
      so access rights must be imposed on any key file.
    * An optional server side CA-chain can be supplied.
    * An optional mechanism for verifying every calling client
      can be activated at server startup time.
    * The cipher priority for handshakes, transmission, etcetera,
      is set to a default value NORMAL, but is configurable when
      starting the server.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Wed, 17 Mar 2010 15:11:57 +0100

webfs (1.21+ds1-1) unstable; urgency=low

  Webfs has been repackaged to use format "3.0 (quilt)" and to NOT use
  the tarball-in-tarball upstream source model.

  The preferred log file is located in '/var/log/webfs/webfs.log',
  and the premade logrotation is tailored to this location.

  The default configuration file suggests or has been extended to
  include the following items:
  
    * Ownership 'www-data:www-data' for the runtime daemon.
    * A new item chooses between buffered or unbuffered logging.
    * A path to CGI-executables is queried.
    * An extra field to include arbitrary additional options.
      This field is included verbatim by the init-script.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Tue, 02 Feb 2010 10:45:24 +0100

